<?php

namespace edu\wisc\doit;

/**
 * Implementation of {@link UserDetailsAttributeMapper} for use in local development.
 */
class LocalUserDetailsAttributeMapper implements UserDetailsAttributeMapper
{

    /**
     * {@inheritdoc}
     */
    public function mapUser()
    {
        $jsonString = file_get_contents(__DIR__ . "/../../../resources/localuser.json");
        if ($jsonString === false) {
            return null;
        }

        // Return user attributes into a standard PHP array (true specifies array)
        return json_decode($jsonString, true);
    }

}