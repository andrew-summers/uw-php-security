<?php

namespace edu\wisc\doit;

/**
 * Implementation of {@link UserDetailsService} for use in local development.
 */
class LocalUserDetailsService implements UserDetailsService
{

    /** @var UserDetailsAttributeMapper */
    private $attributeMapper;
    
    /**
     * LocalUserDetailsService constructor.
     * @param UserDetailsAttributeMapper $mapper
     */
    public function __construct(UserDetailsAttributeMapper $mapper = null)
    {
        if ($mapper == null) {
            $this->attributeMapper = new LocalUserDetailsAttributeMapper();
        } else {
            $this->attributeMapper = $mapper;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function loadUser()
    {
        $userAttributes = $this->attributeMapper->mapUser();

        // Return null if attribute reading failed
        if ($userAttributes == null) {
            return null;
        }
        
        return new UWUserDetails(
            $userAttributes[UserDetailsAttributeMapper::EPPN],
            $userAttributes[UserDetailsAttributeMapper::PVI],
            $userAttributes[UserDetailsAttributeMapper::FULLNAME],
            $userAttributes[UserDetailsAttributeMapper::UDDS],
            $userAttributes[UserDetailsAttributeMapper::EMAIL],
            $userAttributes[UserDetailsAttributeMapper::SOURCE],
            $userAttributes[UserDetailsAttributeMapper::ISIS_EMPLID],
            $userAttributes[UserDetailsAttributeMapper::FIRST_NAME],
            $userAttributes[UserDetailsAttributeMapper::LAST_NAME]
        );
    }
    
}