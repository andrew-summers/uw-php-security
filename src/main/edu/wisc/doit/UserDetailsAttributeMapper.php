<?php

namespace edu\wisc\doit;

/**
 * UserDetailsAttributeMapper defines an interface for mapping common UW user attributes to an associative array. The
 * constants defined in this interface represent headers sent by UW Federated login that identify a user.
 */
interface UserDetailsAttributeMapper
{
    
    // Constants representing UW Federated login Shibboleth headers which should be mapped by concrete implementations.
    const EPPN = "eppn";
    const PVI = "eduWisconsinSPVI";
    const FULLNAME = "eduWisconsinCommonName";
    const FIRST_NAME = "eduWisconsinGivenName";
    const LAST_NAME = "eduWisconsinSurname";
    const EMAIL = "eduWisconsinEmailAddress";
    const UDDS = "udds";
    const SOURCE = "source";
    const ISIS_EMPLID = "isisEmplid";

    /**
     * Map Shibboleth header values to an associative array.
     *
     * @return array
     */
    public function mapUser();

}