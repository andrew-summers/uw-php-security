<?php

namespace edu\wisc\doit;

/**
 * UserDetailsService is the interface that provides an instance of {@link UserDetails}, typically a {@link UWUserDetails}.
 * Two concrete implementations are provided, {@link LocalUserDetailsService} and {@link PreauthUserDetailsService}.
 */
interface UserDetailsService
{

    /**
     * Return a {@link UserDetails} hydrated by a {@link UserDetailsAttributeMapper}, or null if attribute
     * mapping failed.
     *
     * @return UserDetails|null
     */
    public function loadUser();
    
}