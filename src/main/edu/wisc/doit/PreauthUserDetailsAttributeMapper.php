<?php

namespace edu\wisc\doit;

/**
 * Default implementation of {@UserDetailsAttributeMapper} for use in preauthenticated (Shibboleth) environments.
 */
class PreauthUserDetailsAttributeMapper implements UserDetailsAttributeMapper
{

    /**
     * {@inheritdoc}
     */
    public function mapUser()
    {
        $userAttributes[UserDetailsAttributeMapper::EPPN] = $_SERVER[UserDetailsAttributeMapper::EPPN];
        $userAttributes[UserDetailsAttributeMapper::PVI] = $_SERVER[UserDetailsAttributeMapper::PVI];
        $userAttributes[UserDetailsAttributeMapper::FULLNAME] = $_SERVER[UserDetailsAttributeMapper::FULLNAME];
        $userAttributes[UserDetailsAttributeMapper::FIRST_NAME] = $_SERVER[UserDetailsAttributeMapper::FIRST_NAME];
        $userAttributes[UserDetailsAttributeMapper::LAST_NAME] = $_SERVER[UserDetailsAttributeMapper::LAST_NAME];
        $userAttributes[UserDetailsAttributeMapper::EMAIL] = $_SERVER[UserDetailsAttributeMapper::EMAIL];
        $userAttributes[UserDetailsAttributeMapper::UDDS] = $_SERVER[UserDetailsAttributeMapper::UDDS];
        $userAttributes[UserDetailsAttributeMapper::SOURCE] = $_SERVER[UserDetailsAttributeMapper::SOURCE];
        $userAttributes[UserDetailsAttributeMapper::ISIS_EMPLID] = $_SERVER[UserDetailsAttributeMapper::ISIS_EMPLID];

        // Require EPPN, PVI and FULLNAME to be set to consider user loading successful
        if (empty($userAttributes[UserDetailsAttributeMapper::EPPN]) ||
            empty($userAttributes[UserDetailsAttributeMapper::PVI]) ||
            empty($userAttributes[UserDetailsAttributeMapper::FULLNAME])) {
            return null;
        }
        
        return $userAttributes;
    }

}
