<?php

namespace edu\wisc\doit;

/**
 * Default implementation of {@UserDetailsAttributeMapper} for use in preauthenticated (Shibboleth) environments.
 */
class PreauthUserDetailsService implements UserDetailsService
{

    /** @var UserDetailsAttributeMapper */
    private $attributeMapper;

    /**
     * PreauthUserDetailsService constructor.
     * @param UserDetailsAttributeMapper|null $mapper
     */
    public function __construct(UserDetailsAttributeMapper $mapper = null)
    {
        if ($mapper == null) {
            $this->attributeMapper = new PreauthUserDetailsAttributeMapper();
        } else {
            $this->attributeMapper = $mapper;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function loadUser()
    {
        $userAttributes = $this->attributeMapper->mapUser();

        // Return null if attribute reading failed
        if ($userAttributes == null) {
            return null;
        }

        return new UWUserDetails(
            $userAttributes[UserDetailsAttributeMapper::EPPN],
            $userAttributes[UserDetailsAttributeMapper::PVI],
            $userAttributes[UserDetailsAttributeMapper::FULLNAME],
            $userAttributes[UserDetailsAttributeMapper::UDDS],
            $userAttributes[UserDetailsAttributeMapper::EMAIL],
            $userAttributes[UserDetailsAttributeMapper::SOURCE],
            $userAttributes[UserDetailsAttributeMapper::ISIS_EMPLID],
            $userAttributes[UserDetailsAttributeMapper::FIRST_NAME],
            $userAttributes[UserDetailsAttributeMapper::LAST_NAME]
        );
    }

}