<?php

namespace edu\wisc\doit;

/**
 * Class to do basic setup needed to simulate a logged in Shibboleth user.
 */
abstract class PreauthTestCase extends \PHPUnit_Framework_TestCase
{

    /**
     * Populate $_SERVER with Shib attributes to simulate a logged in user
     */
    protected function setUp()
    {
        parent::setUp();
        $jsonString = file_get_contents(__DIR__ . "/../../../resources/testuser.json");
        if ($jsonString === false) {
            return null;
        }

        $attributes = json_decode($jsonString, true);
        $_SERVER[UserDetailsAttributeMapper::EPPN] = $attributes[UserDetailsAttributeMapper::EPPN];
        $_SERVER[UserDetailsAttributeMapper::PVI] = $attributes[UserDetailsAttributeMapper::PVI];
        $_SERVER[UserDetailsAttributeMapper::FULLNAME] = $attributes[UserDetailsAttributeMapper::FULLNAME];
        $_SERVER[UserDetailsAttributeMapper::FIRST_NAME] = $attributes[UserDetailsAttributeMapper::FIRST_NAME];
        $_SERVER[UserDetailsAttributeMapper::LAST_NAME] = $attributes[UserDetailsAttributeMapper::LAST_NAME];
        $_SERVER[UserDetailsAttributeMapper::UDDS] = $attributes[UserDetailsAttributeMapper::UDDS];
        $_SERVER[UserDetailsAttributeMapper::EMAIL] = $attributes[UserDetailsAttributeMapper::EMAIL];
        $_SERVER[UserDetailsAttributeMapper::SOURCE] = $attributes[UserDetailsAttributeMapper::SOURCE];
        $_SERVER[UserDetailsAttributeMapper::ISIS_EMPLID] = $attributes[UserDetailsAttributeMapper::ISIS_EMPLID];
    }

}