<?php

namespace edu\wisc\doit;

/**
 * Tests for {@link LocalUserDetailsService}.
 */
class LocalUserDetailsServiceTest extends \PHPUnit_Framework_TestCase
{

    public function testLoadUser()
    {
        $userDetailsService = new LocalUserDetailsService();
        $user = $userDetailsService->loadUser();
        $this->assertEquals("bbadger@wisc.edu", $user->getEppn());
        $this->assertEquals("UW123A456", $user->getPvi());
        $this->assertEquals("BUCKINGHAM BADGER", $user->getFullName());
        $this->assertEquals("bucky.badger@wisc.edu", $user->getEmailAddress());
        $this->assertEquals("a_source", $user->getSource());
        $this->assertEquals("123456789", $user->getIsisEmplid());
        $this->assertEquals("BUCKINGHAM", $user->getFirstName());
        $this->assertEquals("BADGER", $user->getLastName());
    }
}
