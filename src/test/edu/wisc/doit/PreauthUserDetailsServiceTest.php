<?php

namespace edu\wisc\doit;

/**
 * Tests for {@link PreauthUserDetailsService}.
 */
class PreauthUserDetailsServiceTest extends PreauthTestCase
{

    /** @var UserDetailsService */
    private $userService;

    protected function setUp()
    {
        parent::setUp();
        $this->userService = new PreauthUserDetailsService();
    }

    public function testLoadUser() {
        $user = $this->userService->loadUser();
        $this->assertNotNull($user);
        $this->assertEquals("bbadger@wisc.edu", $user->getEppn());
        $this->assertEquals("UW123A456", $user->getPvi());
        $this->assertEquals("BUCKINGHAM BADGER", $user->getFullName());
        $this->assertEquals("bucky.badger@wisc.edu", $user->getEmailAddress());
        $this->assertEquals("a_source", $user->getSource());
        $this->assertEquals("123456789", $user->getIsisEmplid());
        $this->assertEquals("BUCKINGHAM", $user->getFirstName());
        $this->assertEquals("BADGER", $user->getLastName());
    }

    public function testLoadUserWithNoEPPN() {
        // Clear EPPN to simulate no EPPN
        $_SERVER[UserDetailsAttributeMapper::EPPN] = null;
        $user = $this->userService->loadUser();
        $this->assertNull($user);
    }

}
