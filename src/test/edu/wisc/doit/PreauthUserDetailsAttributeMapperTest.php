<?php

namespace edu\wisc\doit;

/**
 * Tests for {@link PreauthUserDetailsAttributeMapper}.
 */
class PreauthUserDetailsAttributeMapperTest extends PreauthTestCase
{

    public function testMapUser() {
        $attributeMapper = new PreauthUserDetailsAttributeMapper();
        $userAttributes = $attributeMapper->mapUser();
        $this->assertEquals("bbadger@wisc.edu", $userAttributes[UserDetailsAttributeMapper::EPPN]);
        $this->assertEquals("UW123A456", $userAttributes[UserDetailsAttributeMapper::PVI]);
        $this->assertEquals("BUCKINGHAM BADGER", $userAttributes[UserDetailsAttributeMapper::FULLNAME]);
        $this->assertEquals("bucky.badger@wisc.edu", $userAttributes[UserDetailsAttributeMapper::EMAIL]);
        $this->assertEquals("a_source", $userAttributes[UserDetailsAttributeMapper::SOURCE]);
        $this->assertEquals("123456789", $userAttributes[UserDetailsAttributeMapper::ISIS_EMPLID]);
        $this->assertEquals("BUCKINGHAM", $userAttributes[UserDetailsAttributeMapper::FIRST_NAME]);
        $this->assertEquals("BADGER", $userAttributes[UserDetailsAttributeMapper::LAST_NAME]);
        $this->assertEquals(["UW123A456", "UW234A567"], $userAttributes[UserDetailsAttributeMapper::UDDS]);
    }

}
